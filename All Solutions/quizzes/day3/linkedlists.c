#include <stdio.h>
#include <stdlib.h>

struct Node
{
     int data;
     struct Node *next;
};

int main(){

  int n, i;
  scanf("%d", &n);
  struct Node* head = (struct Node*)malloc(sizeof(struct Node));
  struct Node* current = head;
  int temp;
  scanf("%d", &temp);
  current->data = temp;

  for(i = 1; i < n; i++){
    current -> next = (struct Node*)malloc(sizeof(struct Node));
    current = current -> next;
    scanf("%d", &temp);
    current -> data = temp;
    current -> next = NULL;
  }

  struct Node *previous, *next;
  previous = NULL;
  current = head;
  next = current->next;
  while(next != NULL) {
    current->next = previous;
    previous = current;
    current = next;
    next = next->next;
  } 
  current->next = previous;

  while(current != NULL){
  	printf("%d", current->data);
  	current = current->next;
    if(current != NULL) printf(" ");
  }
  printf("\n");
  return 0;
}