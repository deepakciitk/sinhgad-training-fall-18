#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


int markFields(int fields[200][200], int currentRow, int currentColumn, int rows, int columns, int fieldNumber){
    if(!(currentRow >= 0  && currentColumn >= 0
        && currentRow < rows && currentColumn < columns
        && fields[currentRow][currentColumn] == 1)) 
        return 0;

    fields[currentRow][currentColumn] = fieldNumber + 100;
    markFields(fields, currentRow - 1, currentColumn, rows, columns, fieldNumber);
    markFields(fields, currentRow + 1, currentColumn, rows, columns, fieldNumber);
    markFields(fields, currentRow, currentColumn - 1, rows, columns, fieldNumber);
    markFields(fields, currentRow, currentColumn + 1, rows, columns, fieldNumber);
    return 0;
}

long long ncr(int n, int r) {
    if(r > n / 2) r = n - r; // because C(n, r) == C(n, n - r)
    long long ans = 1;
    int i;

    for(i = 1; i <= r; i++) {
        ans *= n - r + i;
        ans /= i;
    }

    return ans;
}

int numberOfCombinations(int numberOfFields){
    int numberOfCombinations = 0;
    int numberOfSheep;
    if(numberOfFields == 0) return 0;
    for (numberOfSheep = 0; numberOfSheep <= numberOfFields; numberOfSheep+=2) {
        numberOfCombinations += ncr(numberOfFields, numberOfSheep);
    }
    return numberOfCombinations;
}

int main() {
    int current, testcases, rows, columns;
    scanf("%d", &testcases);
    int currentRow, currentColumn;
    int fields[200][200];
    int i,j;
    for(current = 0; current < testcases; current++){
        scanf("%d %d", &rows, &columns);
        for(currentRow = 0; currentRow < rows; currentRow++){
            for(currentColumn = 0; currentColumn < columns; currentColumn++){
                scanf("%d", &fields[currentRow][currentColumn]);
            }
        }
        int numberOfFields = 0;
        for(currentRow = 0; currentRow < rows; currentRow++){
            for(currentColumn = 0; currentColumn < columns; currentColumn++){
                if(fields[currentRow][currentColumn] == 1) {
                    numberOfFields++;
                    markFields(fields, currentRow, currentColumn, rows, columns, numberOfFields);
                }
            }
        }
        printf("%d\n", numberOfCombinations(numberOfFields));

    }

    return 0;
}
