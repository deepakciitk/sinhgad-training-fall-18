#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void freqcalc(char str[100000],int freq[26])
{
     int i;
      for(i=0;str[i]!='\0';i++)
          {
               if(str[i]>=65&&str[i]<=90)//capital
               {
                    freq[str[i]-65]++;
               }
                if(str[i]>=97&&str[i]<=122)
               {
                    freq[str[i]-97]++;
               }
          }
}
int main() {

   
     int t,i=0,k;
     scanf("%d",&t);
     for(k=0;k<t;k++)
     {
         char str1[100000],str2[100000];
         int freq1[26],freq2[26];
         for(i=0;i<26;i++)
         {
             freq1[i]=0;freq2[i]=0;
         }
          scanf("%s %s",str1,str2);
          freqcalc(str1,freq1);
          freqcalc(str2,freq2);
          i=0;
          while(i<26)
          {
               if(freq1[i]==freq2[i]) i++;
               else break;
          }
          if(i==26)
               printf("PASS\n");
          else
               printf("FAIL\n");      
               
     }
    return 0;
}