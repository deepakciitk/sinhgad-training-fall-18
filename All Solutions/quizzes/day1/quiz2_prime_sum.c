#include <stdio.h>

int isPrimeNumber(int n);
int isSumOfPrimeNumbers(int n);
int main()
{
    int n;
    scanf("%d", &n);
    printf(isSumOfPrimeNumbers(n)?"true\n":"false\n");
    return 0;
}

int isSumOfPrimeNumbers(int n){
 int i;
 for(i = 2; i <= n/2; ++i)
    {
        if (isPrimeNumber(i) == 1)
        {
            if (isPrimeNumber(n-i) == 1)
            {
                return 1;
            }

        }
    }
    return 0;
}

int isPrimeNumber(int n)
{
    int i, isPrime = 1;

    for(i = 2; i <= n/2; ++i)
    {
        if(n % i == 0)
        {
            return 0;
        }  
    }

    return 1;
}

