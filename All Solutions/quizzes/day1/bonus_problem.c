// SOLUTION USING GCD

#include <stdio.h>
int gcd(int a,int b)
{
    if(b==0)
        return a;
    return gcd(b,a%b);
}

int main() 
{
	int t,a,b,r;
	scanf("%d",&t);
	while(t--)
	{
	    scanf("%d%d",&a,&b);
	    //using MACRO and determining GCD of p,q
	    int k=gcd((a>b)?a:b,(a<b)?a:b);
	    
	    // bring the numbers to their smallest possible forms
	    a/=k; 
	    b/=k;
	    int A[b+5];
	    int B[b+5];
	    int x=0,ans=-1;
	    int q=b+2;
	    for(q=b+2;q>=0;--q)
			A[q]=0;
	    int flag=1;
	    a=a%b;
	    while(1)
	    {
	        if(a==0)
	        {		
	            flag=0;
	            break;
	        }
	        a*=10;
	        while(a<b)
	        {
	            a*=10;
	            x++;
	        }
	        r=a%b;
	        if(A[r]!=0)
	        {
	            ans=x-A[r];
	            break;
	        }
	        A[r]=x;
	        x++;
	        a=r;
	    }
	    if(!flag)
	        printf("-1");
	    else
	        printf("%d",ans);
	}
	return 0;
}

