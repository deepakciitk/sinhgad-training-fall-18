#include<stdio.h>
#include<stdlib.h>
int main(){
	int i,n;
	scanf("%d",&n);
	int *a=(int *)malloc(n*sizeof(int));
	for(i=0;i<n;i++) scanf("%d",&a[i]);
	int max_so_far = -100000000, max_ending_here = 0; 

	for (i = 0; i < n; i++){
		max_ending_here = max_ending_here + a[i];
		if (max_so_far < max_ending_here)
			max_so_far = max_ending_here;
						 
		if (max_ending_here < 0)
			max_ending_here = 0;
	}
	printf("%d\n",max_so_far);
	return 0;
}
