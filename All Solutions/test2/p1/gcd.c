#include<stdio.h>

#define SIZE 10000
int gcd(int a,int b){
	int old_a,old_b;
	old_a=a;
	old_b=b;
	b=a%b;
	a=b;
	if(b==0){
		return old_b;
	}else{
		return gcd(a,b);
	}
	
}

int main(){

	int t,n;
	scanf("%d",&t);

	while(t--){
		scanf("%d",&n);
		int i,a[SIZE];
		for(i=0;i<n;i++){
			scanf("%d", &a[i]);
		}
		int prev_gcd=a[0];
		int next;
		for(i=1;i<n;i++){
			next=a[i];
			prev_gcd=prev_gcd>next ? gcd(prev_gcd,next): gcd(next,prev_gcd);
		}
		
		printf("%d\n",prev_gcd);
		
	}
}
