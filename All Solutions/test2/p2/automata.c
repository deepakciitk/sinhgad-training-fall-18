#include<stdio.h>
#include<stdlib.h>

int main(){
	int r,c,i,j,time;
	scanf("%d",&time);
	scanf("%d",&r);
	scanf("%d",&c);
	int **A = (int **)malloc(r * sizeof(int *));
	int **B = (int **)malloc(r * sizeof(int *));
	for(i=0;i<r;i++){
		A[i]=(int *)malloc(c * sizeof(int));
		B[i]=(int *)malloc(c * sizeof(int));
		for(j=0;j<c;j++){
			scanf("%d",&A[i][j]);
		}
	}
	int flip=0;
	int **C;
	int **temp;
	while(time--){
		
		if(flip==0){
			C=A;temp=B;flip=1;
		}else{
			C=B;temp=A;flip=0;
		}
		for(i=0;i<r;i++){
			for(j=0;j<c;j++){
				int a[8];
				int k;
				for(k=0;k<8;k++){ a[k]=-1; }
				if(i>0 && j>0) 
					a[0]=C[i-1][j-1];

				if(j>0) 
					a[1]=C[i][j-1];

				if(i<r-1 && j>0) 
					a[2]=C[i+1][j-1];
				
				if(i>0) 
					a[3]=C[i-1][j];
				
				if(i<r-1)
					a[4]=C[i+1][j];
				
				if(i>0 && j<c-1)
					a[5]=C[i-1][j+1];
				
				if(j<c-1)
					a[6]=C[i][j+1];
				
				if(i<r-1 && j< c-1)
					a[7]=C[i+1][j+1];
				
				int inactive_n=0;
				int active_n=0;
				for(k=0;k<8;k++){
					if(a[k]==1){active_n++;}
					else if(a[k]==0){inactive_n++;}
				}
				
				if(active_n>=inactive_n){
					temp[i][j]=1;
				}else{
					temp[i][j]=0;
				}
			}
		}
	}

	for(i=0;i<r;i++){
		for(j=0;j<c;j++){
			if(j!=0) printf(" ");
			printf("%d",temp[i][j]);
		}
		printf("\n");
	}
	return 0;
}
