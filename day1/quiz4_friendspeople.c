#include <stdio.h>
#include <stdlib.h>

typedef struct Person
{
     int age;
     struct Person *friend;
} Person;

int findAbove20(Person *head);

int main(){
  int n, i;
  scanf("%d", &n);
  struct Person* head = (struct Person*)malloc(sizeof(struct Person));
  struct Person* current = head;
  int temp;
  scanf("%d", &temp);
  current->age = temp;

  for(i = 1; i < n; i++){
    current -> friend = (struct Person*)malloc(sizeof(struct Person));
    current = current -> friend;
    scanf("%d", &temp);
    current -> age = temp;
    current -> friend = NULL;
  }
  printf("%d\n",findAbove20(head));
}


int findAbove20(Person *head){
  int count=0;
  Person *p = head;
  
  while(p!=NULL)
  {
        if(p -> age > 20)
        count++;
        
        p = p -> friend;
  }
   return count;
}
