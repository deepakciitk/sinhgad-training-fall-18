#include<stdio.h>

int main(){
	int lines;
	scanf("%d",&lines);
	int ignore_newline=1;// flag to ignore the first newline
	int words=0;
	int c,last_char;
	do{
		c=getchar();
		if(c=='\n' && ignore_newline==1){
			// ignore first newline we will encouter
			ignore_newline=0;
			last_char=c;
			continue;
		}else if(c=='\n' && last_char!=' ' && last_char!='\n'){
			//if newline is enountered and last character wasn't a space or newline(empty line) increase word count and print
			words++;
			printf("%d\n",words);
			words=0;
		}else if(c=='\n' && last_char==' '){
			//if newline is enountered and last character was a space just print the number of words
			printf("%d\n",words);
			words=0;
		}else if(c=='\n' && last_char=='\n'){
			// meaning empty line
			printf("%d\n",0);
		}else if(c==' ' && last_char!=' ' && last_char!='\n'){
			// incease word count when you encounter a space
			words++;
		}else if(c==EOF && last_char!=' ' && last_char!='\n'){
			words++;
			printf("%d\n",words);
		}

		last_char=c;
	}while(c!=EOF);

	return 0;
}
